package com.example.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CustomItem extends FrameLayout {

    private Context mContext;
    private View mView;
    private ImageView mItemImage;
    private TextView mItemText;

    public CustomItem(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public CustomItem(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomItem(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public CustomItem(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        mContext = context;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.custom_item, this, true);

        mItemImage = findViewById(R.id.item_image);
        mItemText = findViewById(R.id.item_text);

        TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.CustomItem);
        mItemText.setText(a.getString(R.styleable.CustomItem_custom_item_text));
        mItemImage.setImageResource(a.getResourceId(R.styleable.CustomItem_custom_item_image, R.drawable.image3));
    }

}
