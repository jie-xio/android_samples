#include <jni.h>
#include <string>

// Write C++ code here.
//
// Do not forget to dynamically load the C++ library into your application.
//
// For instance,
//
// In MainActivity.java:
//    static {
//       System.loadLibrary("testtinker");
//    }
//
// Or, in MainActivity.kt:
//    companion object {
//      init {
//         System.loadLibrary("testtinker")
//      }
//    }
extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_testtinker_MainActivity_stringFromCpp(JNIEnv *env, jobject thiz) {
    // TODO: implement stringFromCpp()
    std::string strhello = "hello world! c++ ni hao ya ddd";
    return env->NewStringUTF(strhello.c_str());
}
extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_testtinker_MainActivity_stringOther(JNIEnv *env, jobject thiz) {
    // TODO: implement stringOther()
    std::string strhello = "其他语句，hahahah";
    return env->NewStringUTF(strhello.c_str());
}