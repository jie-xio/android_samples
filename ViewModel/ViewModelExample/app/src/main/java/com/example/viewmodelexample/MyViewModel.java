package com.example.viewmodelexample;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MyViewModel extends ViewModel {

    private MutableLiveData<Integer> mProgress;

    public MutableLiveData<Integer> getProgress(){
        if (mProgress == null){
            mProgress = new MutableLiveData<>();
            mProgress.setValue(0);
        }
        return mProgress;
    }

}
