package com.example.recyclerview;

import java.util.ArrayList;
import java.util.List;

public class BookUtils {
    public static List<Book> get(){
        List<Book> bookList = new ArrayList<>();

        Book book1 = new Book("浪潮之巅", "吴军", "https://img9.doubanio.com/view/subject/l/public/s6807265.jpg");
        bookList.add(book1);

        Book book2 = new Book("从0到1", "蒂尔、马斯特斯", "https://img9.doubanio.com/view/subject/l/public/s28299265.jpg");
        bookList.add(book2);

        Book book3 = new Book("硅谷钢铁侠", "阿什利·万斯", "https://img9.doubanio.com/view/subject/l/public/s28571694.jpg");
        bookList.add(book3);

        Book book4 = new Book("重构：改善既有代码的设计", "Martin Fowler", "https://img2.doubanio.com/view/subject/l/public/s30014452.jpg");
        bookList.add(book4);

        Book book5 = new Book("代码整洁之道", "Robert C. Martin", "https://img2.doubanio.com/view/subject/l/public/s4103991.jpg");
        bookList.add(book5);

        Book book6 = new Book("代码大全", "史蒂夫·迈克康奈尔", "https://img1.doubanio.com/view/subject/l/public/s1495029.jpg");
        bookList.add(book6);

        return bookList;
    }
}
