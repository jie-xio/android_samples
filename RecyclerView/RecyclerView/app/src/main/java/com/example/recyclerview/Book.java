package com.example.recyclerview;

public class Book {
    public String bookName;
    public String bookAuthor;
    public String bookImage;

    public Book(String bookName, String bookAuthor, String bookImage) {
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.bookImage = bookImage;
    }
}

