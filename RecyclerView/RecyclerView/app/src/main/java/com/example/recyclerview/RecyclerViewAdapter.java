package com.example.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview.databinding.RecyclerViewItemBinding;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    List<Book> bookList;

    public RecyclerViewAdapter(List<Book> bookList) {
        this.bookList = bookList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerViewItemBinding recyclerViewItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_item, parent, false);
        return new MyViewHolder(recyclerViewItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.recyclerViewItemBinding.setBook(bookList.get(position));
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        private RecyclerViewItemBinding recyclerViewItemBinding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public MyViewHolder(RecyclerViewItemBinding recyclerViewItemBinding) {
            super(recyclerViewItemBinding.getRoot());
            this.recyclerViewItemBinding = recyclerViewItemBinding;
        }
    }
}
