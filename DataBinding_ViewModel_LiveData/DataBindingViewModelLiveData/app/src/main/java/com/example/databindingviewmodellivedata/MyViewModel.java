package com.example.databindingviewmodellivedata;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MyViewModel extends ViewModel {
    private MutableLiveData<Integer> aTeamScore;
    private MutableLiveData<Integer> bTeamScore;

    private int mATeamLastScore;
    private int mBTeamLastScore;

    public MutableLiveData<Integer> getATeamScore() {
        if (aTeamScore == null) {
            aTeamScore = new MutableLiveData<>();
            aTeamScore.setValue(0);
        }
        return aTeamScore;
    }

    public MutableLiveData<Integer> getBTeamScore() {
        if (bTeamScore == null) {
            bTeamScore = new MutableLiveData<>();
            bTeamScore.setValue(0);
        }
        return bTeamScore;
    }

    public void addScoreToA(int i) {
        saveLastScore();
        aTeamScore.setValue(aTeamScore.getValue() + i);
    }

    public void addScoreToB(int i) {
        saveLastScore();
        bTeamScore.setValue(bTeamScore.getValue() + i);
    }

    public void resetScore() {
        aTeamScore.setValue(0);
        bTeamScore.setValue(0);
    }

    public void undoScore() {
        aTeamScore.setValue(mATeamLastScore);
        bTeamScore.setValue(mBTeamLastScore);
    }

    private void saveLastScore() {
        mATeamLastScore = aTeamScore.getValue();
        mBTeamLastScore = bTeamScore.getValue();
    }
}
