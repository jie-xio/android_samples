package com.example.databinding2;

import android.util.Log;

import androidx.databinding.ObservableField;

public class UserViewModel {
    private ObservableField<User> userObservableField;

    public UserViewModel() {
        User user = new User("Jack");
        userObservableField = new ObservableField<>();
        userObservableField.set(user);
    }

    public String getUserName(){
        return userObservableField.get().userName;
    }

    public void setUserName(String userName){
        Log.d("UserViewModel", "userObservableField: " + userName);
        userObservableField.get().userName = userName;
    }
}
