package com.example.databinding;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

public class ButtonHandleListener {
    private Context context;

    public ButtonHandleListener(Context context) {
        this.context = context;
    }

    public void buttonOnClick(View view){
        Toast.makeText(context, "想要", Toast.LENGTH_SHORT).show();
    }
}
