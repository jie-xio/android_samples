package com.example.databinding;

public class Book {
    public String bookName;
    public String bookAuthor;
    public int star;
    public int bookImageRes;


    public Book(String bookName, String bookAuthor, int star, int bookImageRes) {
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.star = star;
        this.bookImageRes = bookImageRes;
    }
}
