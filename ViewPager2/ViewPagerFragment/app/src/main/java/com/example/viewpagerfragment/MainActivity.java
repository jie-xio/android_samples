package com.example.viewpagerfragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager2 viewPager2 = findViewById(R.id.viewpager);

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(BlankFragment.newInstance("第一个页面"));
        fragmentList.add(BlankFragment.newInstance("第二个页面"));
        fragmentList.add(BlankFragment.newInstance("第三个页面"));
        fragmentList.add(BlankFragment.newInstance("第四个页面"));
        fragmentList.add(BlankFragment.newInstance("第五个页面"));

        viewPager2.setAdapter(new MyFragmentStateAdapter(getSupportFragmentManager(), getLifecycle(), fragmentList));
    }
}