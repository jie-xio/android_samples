package com.example.testviewpager;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends RecyclerView.Adapter<ViewPagerAdapter.ViewPagerViewHolder> {
    private List<String> mTitles = new ArrayList<>();
    private List<Integer> mColors = new ArrayList<>();

    public ViewPagerAdapter(){
        mTitles.add("你好0");
        mTitles.add("你好1");
        mTitles.add("你好2");
        mTitles.add("你好3");
        mTitles.add("你好4");
        mTitles.add("你好5");
        mTitles.add("你好6");
        mTitles.add("你好7");
        mTitles.add("你好8");
        mTitles.add("你好9");

        mColors.add(Color.parseColor("#00ff00"));
        mColors.add(Color.parseColor("#55ff11"));
        mColors.add(Color.parseColor("#00ff22"));
        mColors.add(Color.parseColor("#66ff00"));
        mColors.add(Color.parseColor("#003300"));
        mColors.add(Color.parseColor("#22ff00"));
        mColors.add(Color.parseColor("#992200"));
        mColors.add(Color.parseColor("#11ff88"));
        mColors.add(Color.parseColor("#69ff33"));
        mColors.add(Color.parseColor("#00ff77"));
    }


    @NonNull
    @Override
    public ViewPagerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewPagerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pager, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewPagerViewHolder holder, int position) {
        holder.mItemPagerTv.setText(mTitles.get(position));
        //holder.mItemPagerContainer.setBackgroundColor(mColors.get(position));
    }

    @Override
    public int getItemCount() {
        return mTitles.size();
    }


    class ViewPagerViewHolder extends RecyclerView.ViewHolder{

        FrameLayout mItemPagerContainer;
        TextView mItemPagerTv;

        public ViewPagerViewHolder(@NonNull View itemView) {
            super(itemView);

            mItemPagerContainer = itemView.findViewById(R.id.item_pager_container);
            mItemPagerTv = itemView.findViewById(R.id.item_pager_tv);
        }
    }
}
