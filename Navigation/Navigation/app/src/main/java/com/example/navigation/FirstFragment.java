package com.example.navigation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class FirstFragment extends Fragment {

    private View mFragmentView;
    private Button mBtnGoToSecondFragment;

    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mFragmentView == null){
            mFragmentView = inflater.inflate(R.layout.fragment_first, container, false);

            mBtnGoToSecondFragment = mFragmentView.findViewById(R.id.btn_go_to_second);
            mBtnGoToSecondFragment.setOnClickListener((view) -> {
                NavController navController = Navigation.findNavController(view);
                navController.navigate(R.id.action_firstFragment_to_secondFragment);
            });
        }

        return mFragmentView;
    }

}